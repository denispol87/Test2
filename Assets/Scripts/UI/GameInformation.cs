using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class GameInformation : MonoBehaviour
{
    private TextMeshProUGUI text;  
    private int seconds = 0;
    private List<Warrior> allWarriors = new List<Warrior>();
    private void Start()
    {
        text = GetComponent<TextMeshProUGUI>();        
        var warriors = GameObject.FindGameObjectsWithTag("Warrior");
        foreach (var item in warriors)
        {            
            allWarriors.Add(item.GetComponent<Warrior>());
        }
        StartCoroutine(TimeFlow());
    }
    private IEnumerator TimeFlow()
    {        
        yield return new WaitForSeconds(1);
        StartCoroutine(TimeFlow());
        seconds++;
        text.text = "Time game: " + seconds + " sec.";
        CheckWin();     
    }
    private void CheckWin()
    {
        TypeTeam winner = TypeTeam.none;
        foreach (var warrior in allWarriors)
        {            
            if (!warrior.IsDead)
            {             
                if (winner == TypeTeam.none)
                {
                    winner = warrior.TypeTeam;
                }
                else if (winner != warrior.TypeTeam)
                {
                    //��� ��� ����������
                    return;
                }
            }
        }
        StopAllCoroutines();
        switch (winner)
        {
            case TypeTeam.none:
                text.text = "Time game: "+seconds + " sec. No one won!";
                break;
            case TypeTeam.red:
                text.text = "Time game: " + seconds + " sec. The reds won!";
                break;
            case TypeTeam.blue:
                text.text = "Time game: " + seconds + " sec. The blues won!";
                break;
        }
    }
}
