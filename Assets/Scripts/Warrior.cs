using StatePattern;
using System;
using UnityEngine;

public class Warrior : MonoBehaviour
{
    [Header("Initial Parameters")]
    public int HP = 100;
    public int maxHP = 100;
    public int minHP = 0;
    public float SpeedMove = 1;
    public float RadiusAttack;
    public int Damage;

    
    public float SpeedRotation = 120;
    public TypeTeam TypeTeam = TypeTeam.red;
    public bool IsDead = false;

    [Header("States")]
    public State StartState;
    public State FindTargetState;
    public State MoveAttackPositionState;
    public State AttackState;
    public State DeadState;

    [Header("Actual state")]
    public State CurrentState;

    [SerializeField] private Warrior enemyWarrior;

    private void Start()
    {
        SetState(StartState);
    }
    private void FixedUpdate()
    {     
        if(!CurrentState.IsFinished)
        {
            CurrentState.Run();
        }
        else
        {
            if (enemyWarrior != null && enemyWarrior.IsDead)
                enemyWarrior = null;

            if (enemyWarrior == null)
                SetState(FindTargetState);            
            else if (!IsAttackDistance())
                SetState(MoveAttackPositionState);                
            else
                SetState(AttackState);
        }
    }
    public void SetState(State state)
    {
        if (CurrentState != null && !CurrentState.IsFinished)
            CurrentState.Stop();

        CurrentState = Instantiate(state);
        CurrentState.Warrior = this;
        CurrentState.Init();
    }
    private bool IsAttackDistance()
    {
        if (enemyWarrior != null)
        {
            if(Vector3.Distance(enemyWarrior.transform.position,transform.position)<=RadiusAttack)            
                return true;            
            else
                return false;
        }
        else
        {
            return false;
        }
    }
    public void SetEnemy(Warrior warrior)
    {
        enemyWarrior = warrior;        
    }
    public void SetDamage(Warrior attacking, int damage)
    {
        HP = Mathf.Clamp(HP - damage, minHP, maxHP);
        if (HP == minHP)
            SetState(DeadState);
    }
    public Warrior GetEnemy()
    {
        return enemyWarrior;
    }

    public void MoveTo(Vector3 position)
    {
        position.y = transform.position.y;

        transform.position = Vector3.MoveTowards(transform.position, position, SpeedMove);        
    }
    public void RotateTo(Vector3 position)
    {             
        transform.rotation = Quaternion.RotateTowards(transform.rotation, Quaternion.LookRotation(position - transform.position), SpeedRotation);
    }
    public void SetMaterial(Material deathMaterial)
    {
        GetComponent<MeshRenderer>().material = deathMaterial;
    }

}
public enum TypeTeam
{
    none = 0,
    red = 1,
    blue = 2,
}

