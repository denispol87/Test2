using StatePattern;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "FindTargetState", menuName = "States/FindTargetState")]
public class FindTargetState : State
{        
    private List<Warrior> enemyWarriors = new List<Warrior>();
    public override void Init()
    {
        var warriors = GameObject.FindGameObjectsWithTag("Warrior");
        foreach (var item in warriors)
        {
            Warrior warrior = item.GetComponent<Warrior>();
            if (warrior.TypeTeam != Warrior.TypeTeam && !warrior.IsDead)
                enemyWarriors.Add(warrior);
        }
        if (enemyWarriors.Count == 0)
        {
            IsFinished = true;
            return;
        }
        else
        {
            var enemy = ChoiceEnemy();
            Warrior.SetEnemy(enemy);
            Warrior.RotateTo(enemy.transform.position);
            IsFinished = true;
        }
    }
    private Warrior ChoiceEnemy()
    {
        //search for the nearest enemy
        Warrior target = enemyWarriors[0];
        float distance = Vector3.Distance(target.transform.position, Warrior.transform.position);
        for (int i = 1; i < enemyWarriors.Count; i++)
        {          

            float dist = Vector3.Distance(enemyWarriors[i].transform.position, Warrior.transform.position);
            if (distance > dist)
            {
                distance = dist;
                target = enemyWarriors[i];
            }
        }
        return target;
    }
    public override void Run()
    {
        if (IsFinished)
            return;
        
    }
}
