using StatePattern;
using System;
using System.Collections;
using UnityEngine;

[CreateAssetMenu(fileName = "ReloadState", menuName = "States/ReloadState")]
public class ReloadState : State
{
    public int ReloadTimeInSeconds = 1;    
    public override void Init()
    {
        StartCoroutine(TimerAction(ReloadTimeInSeconds, FinishReload));
    }
    public override void Run()
    {
        if (IsFinished)
            return;

        Warrior.RotateTo(Warrior.GetEnemy().transform.position);
    }    
    private void FinishReload()
    {
        IsFinished = true;
    }
    private IEnumerator TimerAction(int seconds, Action finishAction)
    {
        yield return new WaitForSeconds(seconds);
        finishAction?.Invoke();
    }
}
