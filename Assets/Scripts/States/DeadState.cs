using StatePattern;
using UnityEngine;

[CreateAssetMenu(fileName = "DeadState", menuName = "States/DeadState")]
public class DeadState : State
{
    public Material DeathMaterial;
    public override void Init()
    {
        Warrior.IsDead = true;
        Warrior.SetMaterial(DeathMaterial);
    }
    public override void Run()
    {
        if (IsFinished)
            return;

        //�� ������� ���������� ����� ����� ��������� ���� ���������, ��������, ���������
    }
}
