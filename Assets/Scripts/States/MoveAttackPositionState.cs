using StatePattern;
using UnityEngine;

[CreateAssetMenu(fileName = "MoveAttackPositionState", menuName = "States/MoveAttackPositionState")]
public class MoveAttackPositionState : State
{
    public override void Run()
    {
        if (IsFinished)
            return;

        MoveToTarget();
    }
    private void MoveToTarget()
    {
        if(Warrior.GetEnemy().IsDead)
        {
            IsFinished = true;
            return;
        }

        var distance = Vector3.Distance(Warrior.GetEnemy().transform.position, Warrior.transform.position);
        if (distance <= Warrior.RadiusAttack)
        {
            IsFinished = true;
        }
        else
        {
            Warrior.MoveTo(Warrior.GetEnemy().transform.position);
            Warrior.RotateTo(Warrior.GetEnemy().transform.position);
        }
    }
}
