using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace StatePattern
{
    public abstract class State : ScriptableObject
    {
#if UNITY_EDITOR
        [Multiline]
        public string DeveloperDescription = "";
#endif
        public bool IsFinished { get; protected set; }
        [HideInInspector] public Warrior Warrior;

        private List<Coroutine> coroutins = new List<Coroutine>();
        public virtual void Init() { }
        public abstract void Run();
        public virtual void Stop() {
            foreach (var coroutine in coroutins)
            {
                Warrior.StopCoroutine(coroutine);
            }            
        }
        protected Coroutine StartCoroutine(IEnumerator enumerator)
        {
            var coroutine = Warrior.StartCoroutine(enumerator);
            coroutins.Add(coroutine);
            return coroutine;
        }
    }
}
