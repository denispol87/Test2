using StatePattern;
using UnityEngine;

[CreateAssetMenu(fileName = "AttackState", menuName = "States/AttackState")]
public class AttackState : State
{
    public State ReloadState;
    public override void Run()
    {
        if (IsFinished)
            return;

        Attack();
    }
    private void Attack()
    {        
        var enemy = Warrior.GetEnemy();
        if (!enemy.IsDead)
        {        
            Warrior.RotateTo(Warrior.GetEnemy().transform.position);
            enemy.SetDamage(Warrior, Warrior.Damage);
            Warrior.SetState(ReloadState);
        }        
        IsFinished = true;
    }
}
